//
//  User.swift
//  RealmRestSync
//
//  Created by Stefan Kofler on 26.05.18.
//  Copyright © 2018 QuickBird Studios GmbH. All rights reserved.
//

import RealmSwift

class User: Object, Uploadable {
    @objc dynamic var id: Int = 0
    @objc dynamic var username: String = ""
    @objc dynamic var updatedDate: Date = Date()

    override static func primaryKey() -> String? {
        return "id"
    }
    
    static var resourceURL: URL {
        return URL(string: "\(API)/users")!
    }
}
